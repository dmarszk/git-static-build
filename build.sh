#!/bin/bash

VER_GIT=2.9.5
VER_ZLIB=1.2.11
VER_SSL=1.1.1k
VER_CURL=7.77.0
mkdir -p static-git-libs downloads src
MAIN_DIR=`pwd`
echo "Main directory: $MAIN_DIR"
DOWNLOAD_DIR="$MAIN_DIR/downloads"
SRC_DIR="$MAIN_DIR/src"
LIBS_DIR=`readlink -f $MAIN_DIR/static-git-libs`
INSTALL_DIR="/opt/git-$VER_GIT"
export CC=musl-gcc
export CFLAGS="-I$LIBS_DIR/include -static"
export CXXFLAGS="-I$LIBS_DIR/include -static"
export LDFLAGS="-L$LIBS_DIR/lib -static"

curl -C - -L https://zlib.net/zlib-$VER_ZLIB.tar.xz -o $DOWNLOAD_DIR/zlib-$VER_ZLIB.tar.xz
curl -C - -L https://www.openssl.org/source/openssl-$VER_SSL.tar.gz -o $DOWNLOAD_DIR/openssl-$VER_SSL.tar.gz
curl -C - -L https://curl.se/download/curl-$VER_CURL.tar.xz -o $DOWNLOAD_DIR/curl-$VER_CURL.tar.xz
cd $SRC_DIR
tar -xf $DOWNLOAD_DIR/openssl-$VER_SSL.tar.gz
tar -xf $DOWNLOAD_DIR/zlib-$VER_ZLIB.tar.xz
tar -xf $DOWNLOAD_DIR/curl-$VER_CURL.tar.xz
git clone https://github.com/git/git.git --depth 1 --branch v$VER_GIT git


cd $SRC_DIR/openssl-$VER_SSL
make distclean
./Configure --prefix=$LIBS_DIR linux-x86_64 no-shared no-async no-engine -DOPENSSL_NO_SECURE_MEMORY
make -j`nproc`
make install

cd $SRC_DIR/zlib-$VER_ZLIB
make distclean
PKG_CONFIG="pkg-config --static" ./configure --prefix=$LIBS_DIR --static
make -j`nproc`
make install

cd $SRC_DIR/curl-$VER_CURL
make distclean
PKG_CONFIG="pkg-config --static" ./configure --prefix=$LIBS_DIR --disable-shared --enable-static --disable-ldap --enable-ipv6 --enable-unix-sockets --with-openssl=$LIBS_DIR --with-zlib=$LIBS_DIR
make -j`nproc`
make install

cd $SRC_DIR/git
make distclean
make configure
LIBS="-lssl -lcrypto -lz" PKG_CONFIG="pkg-config --static" ./configure --prefix=$INSTALL_DIR  NO_TCLTK=1 --with-openssl=$LIBS_DIR --with-curl=$LIBS_DIR
make -j`nproc`
sudo make install

tar -czvf $MAIN_DIR/git-$VER_GIT-static-linux-x86_64.tgz $INSTALL_DIR
tar -czvf $MAIN_DIR/git-$VER_GIT-static-linux-x86_64-with-certs.tgz $INSTALL_DIR /etc/ssl/certs/ca-certificates.crt


